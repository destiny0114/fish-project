﻿using UnityEngine;
using System.Collections;

public class Fog : MonoBehaviour 
{

	// Use this for initialization
	void Start ()
	{
		RenderSettings.fogColor = Camera.main.backgroundColor;
		RenderSettings.fogDensity = 0.03f;
		RenderSettings.fog = true;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
